<?php
require_once 'models/users.php';


class usersController{

    private $model;


    public function __CONSTRUCT(){
        $this->model = new users();
    }


    public function Index(){
        require_once 'views/users/users.php';

    }


    public function Crud(){
        $users = new users();

        if(isset($_REQUEST['id'])){
            $users = $this->model->Obtener($_REQUEST['id']);
        }
        require_once 'views/users/users.php';
    }

    public function Guardar(){
        $users = new users();
        $users->id = $_REQUEST['id'];
        $users->email = $_REQUEST['email'];
        $users->nombre = $_REQUEST['nombre'];
        $users->apellido = $_REQUEST['apellido'];
        $users->thema = $_REQUEST['thema'];
        $users->tipo = $_REQUEST['tipo'];
        $users->id > 0
            ? $this->model->Actualizar($users)
            : $this->model->Registrar($users);

        header('Location: index.php', false);
    }

    public function Eliminar(){
        $this->model->Eliminar($_REQUEST['id']);
        header('Location: index.php', false);
    }
}
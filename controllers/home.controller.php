<?php
/**
 * Created by PhpStorm.
 * User: josuedjh
 * Date: 18-06-18
 * Time: 10:52 PM
 */
require_once 'models/home.php';

class homeController{
    public $models;

    public function __CONSTRUCT(){
        $this->model = new home();
    }
    public function Index(){
        require_once 'views/home/home.php';

    }
}

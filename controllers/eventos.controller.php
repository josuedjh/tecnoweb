<?php
require_once 'models/eventos.php';


class eventosController{

    private $model;


    public function __CONSTRUCT(){
        $this->model = new eventos();
    }


    public function Index(){
        require_once 'views/eventos/eventos.php';

    }


    public function Crud(){
        $users = new eventos();

        if(isset($_REQUEST['id'])){
            $eventos = $this->model->Obtener($_REQUEST['id']);
        }
        require_once 'views/eventos/eventos.php';
    }

    public function Guardar(){
        $eventos = new eventos();
        $eventos->id = $_REQUEST['id'];
        $eventos->tipoevento = $_REQUEST['tipoevento'];
        $eventos->titulo = $_REQUEST['titulo'];
        $eventos->detalle = $_REQUEST['detalle'];
        $eventos->tipo = $_REQUEST['tipo'];
        $eventos->id > 0
            ? $this->model->Actualizar($eventos)
            : $this->model->Registrar($eventos);

        header('Location: index.php', false);
    }

    public function Eliminar(){
        $this->model->Eliminar($_REQUEST['id']);
        header('Location: index.php', false);
    }
}
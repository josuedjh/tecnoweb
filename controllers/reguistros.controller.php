<?php
/**
 * Created by PhpStorm.
 * User: josuedjh
 * Date: 18-06-18
 * Time: 10:52 PM
 */
require_once 'models/reguistros.php';
require_once 'models/zonas.php';
require_once 'models/conductores.php';
require_once 'models/users.php';
require_once 'models/horarios.php';

class reguistrosController{
    private $model;
    public $modelszonas;
    public $modelshorarios;
    public $modelsconductores;
    public $modelsusers;

    public function __CONSTRUCT(){
        $this->model = new reguistros();
        $this->modelszonas = new zonas();
        $this->modelsusers = new Users();
        $this->modelshorarios = new horarios();
        $this->modelsconductores = new conductores();
    }
    public function Index(){
        require_once 'views/reguistros/reguistros.php';

    }

    public function obtenerUsers(){
        $reguistros = new Users();
        $reguistros = $this->modelsusers->Listar();
    }

    public function obtenerZonas(){
        $reguistros = new zonas();
        $reguistros = $this->modelszonas->Listar();
    }
    public function obtenerHorarios(){
        $reguistros = new conductores();
        $reguistros = $this->modelshorarios->Listar();
    }
    public function obtenerConductores(){
        $reguistros = new horarios();
        $reguistros = $this->modelsconductores->Listar();
    }
    public function Crud(){
        $reguistros = new reguistros();

        if(isset($_REQUEST['id'])){
            $reguistros = $this->model->Obtener($_REQUEST['id']);
        }
        require_once 'views/reguistros/reguistros.php';
    }
    public function Guardar(){

            $reguistros = new reguistros();
            $reguistros->id = $_REQUEST['id'];

            $reguistros->conductor = $_REQUEST['conductor'];
            $reguistros->users = $_REQUEST['users'];
            $reguistros->horario = $_REQUEST['horario'];
            $reguistros->estado = $_REQUEST['estado'];
            $reguistros->zona = $_REQUEST['zona'];

            $reguistros->id > 0
                ? $this->model->Actualizar($reguistros)
                : $this->model->Registrar($reguistros);

            header('Location: index.php', false);

    }
    public function Eliminar(){
        $this->model->Eliminar($_REQUEST['id']);
        header('Location: index.php', false);
    }
}

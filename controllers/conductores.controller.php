<?php
/**
 * Created by PhpStorm.
 * User: josuedjh
 * Date: 18-06-18
 * Time: 10:52 PM
 */
require_once 'models/conductores.php';
class conductoresController{
    private $model;

    public function __CONSTRUCT(){
        $this->model = new conductores();
    }
    public function Index(){
        require_once 'views/conductores/conductores.php';

    }
    public function Crud(){
        $conductores = new conductores();

        if(isset($_REQUEST['id'])){
            $conductores = $this->model->Obtener($_REQUEST['id']);
        }
        require_once 'views/conductores/conductores.php';
    }
    public function Guardar(){
        $conductores = new conductores();
        $conductores->id = $_REQUEST['id'];
        $conductores->nombre = $_REQUEST['nombre'];
        $conductores->apellido = $_REQUEST['apellido'];
        $conductores->celular = $_REQUEST['celular'];
        $conductores->tipo = $_REQUEST['tipo'];
        $conductores->id > 0
            ? $this->model->Actualizar($conductores)
            : $this->model->Registrar($conductores);

        header('Location: index.php', false);
    }
    public function Eliminar(){
        $this->model->Eliminar($_REQUEST['id']);
        header('Location: index.php', false);
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: josuedjh
 * Date: 18-06-18
 * Time: 10:52 PM
 */
require_once 'models/zonas.php';
class zonasController{
    private $model;

    public function __CONSTRUCT(){
        $this->model = new zonas();
    }
    public function Index(){
        require_once 'views/zonas/zonas.php';

    }
    public function Crud(){
        $zonas = new zonas();

        if(isset($_REQUEST['id'])){
            $zonas = $this->model->Obtener($_REQUEST['id']);
        }
        require_once 'views/zonas/zonas.php';
    }
    public function Guardar(){
        $zonas = new zonas();
        $zonas->id = $_REQUEST['id'];
        $zonas->nombre = $_REQUEST['nombre'];
        $zonas->capasidad = $_REQUEST['capasidad'];
        $zonas->locacion = $_REQUEST['locacion'];
        $zonas->id > 0
            ? $this->model->Actualizar($zonas)
            : $this->model->Registrar($zonas);

        header('Location: index.php', false);
    }
    public function Eliminar(){
        $this->model->Eliminar($_REQUEST['id']);
        header('Location: index.php', false);
    }
}

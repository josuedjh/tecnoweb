<?php
/**
 * Created by PhpStorm.
 * User: josuedjh
 * Date: 18-06-18
 * Time: 10:52 PM
 */
require_once 'models/horarios.php';
class horariosController{
    private $model;

    public function __CONSTRUCT(){
        $this->model = new horarios();
    }
    public function Index(){
        require_once 'views/horarios/horarios.php';

    }
    public function Crud(){
        $horarios = new horarios();

        if(isset($_REQUEST['id'])){
            $horarios = $this->model->Obtener($_REQUEST['id']);
        }
        require_once 'views/horarios/horarios.php';
    }
    public function Guardar(){
        $horarios = new horarios();
        $horarios->id = $_REQUEST['id'];
        $horarios->entrada = $_REQUEST['entrada'];
        $horarios->salidad = $_REQUEST['salidad'];
        $horarios->detalle = $_REQUEST['detalle'];
        $horarios->id > 0
            ? $this->model->Actualizar($horarios)
            : $this->model->Registrar($horarios);

        header('Location: index.php', false);
    }
    public function Eliminar(){
        $this->model->Eliminar($_REQUEST['id']);
        header('Location: index.php', false);
    }
}

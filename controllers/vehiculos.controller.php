<?php
/**
 * Created by PhpStorm.
 * User: josuedjh
 * Date: 18-06-18
 * Time: 10:52 PM
 */
require_once 'models/vehiculos.php';
require_once 'models/conductores.php';

class vehiculosController{
    private $model;
    private $modelsdocntores;

    public function __CONSTRUCT(){
        $this->model = new vehiculos();
        $this->modelsdocntores = new conductores();
    }

    public function obtenerConductores(){
        $vehiculos = new conductores();
        $vehiculos = $this->modelsdocntores->Listar();
        require_once 'views/vehiculos/vehiculos.php';

    }

    public function Index(){
        require_once 'views/vehiculos/vehiculos.php';

    }
    public function Crud(){
        $vehiculos = new vehiculos();

        if(isset($_REQUEST['id'])){
            $vehiculos = $this->model->Obtener($_REQUEST['id']);
        }
        require_once 'views/vehiculos/vehiculos.php';
    }
    public function Guardar(){
        $vehiculos = new vehiculos();
        $vehiculos->id = $_REQUEST['id'];
        $vehiculos->idconductor = $_REQUEST['idconductor'];
        $vehiculos->modelo = $_REQUEST['modelo'];
        $vehiculos->placa = $_REQUEST['placa'];
        $vehiculos->color = $_REQUEST['color'];
        $vehiculos->id > 0
            ? $this->model->Actualizar($vehiculos)
            : $this->model->Registrar($vehiculos);
        header('Location: index.php', false);
    }
    public function Eliminar(){
        $this->model->Eliminar($_REQUEST['id']);
        header('Location: index.php', false);
    }
}

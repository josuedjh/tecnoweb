<?php
include_once ('views/header.php');

error_reporting(0);

session_start();

if( isset($_SESSION['user_id']) ){
    header("Location: /");
}

require 'db.php';

$message = '';

if(!empty($_POST['email']) && !empty($_POST['password'])):

    // Enter the new user in the database
    $sql = "INSERT INTO users (email, password) VALUES (:email, :password)";
    $stmt = $conn->prepare($sql);

    $stmt->bindParam(':email', $_POST['email']);
    $stmt->bindParam(':password', password_hash($_POST['password'], PASSWORD_BCRYPT));

    if( $stmt->execute() ):
        $message = 'Successfully created new user';
    else:
        $message = 'Sorry there must have been an issue creating your account';
    endif;

endif;

?>

<link href="static/css/theme.css" rel="stylesheet" media="all">

<body class="animsition">
<div class="page-wrapper">
    <div class="page-content--bge5">
        <div class="container">
            <div class="login-wrap">
                <div class="login-content">
                    <div class="login-logo">
                        <a href="#">
                            <img src="static/images/icon/logo.png" alt="CoolAdmin">

                        </a>
                        <?php if(!empty($message)): ?>
                            <p><?= $message ?></p>
                        <?php endif; ?>
                    </div>
                    <div class="login-form">
                        <form action="reguister.php" method="POST"">
                            <div class="form-group">
                                <label>Email Address</label>
                                <input class="au-input au-input--full" type="email" name="email" placeholder="Email">
                            </div>
                            <div class="form-group">
                                <label>Password</label>
                                <input class="au-input au-input--full" type="password" name="password" placeholder="Password">
                            </div>
                        <div class="form-group">
                            <label>Password</label>
                            <input class="au-input au-input--full" type="password" name="confirm_password" placeholder="Password repet">
                        </div>
                        <button class="au-btn au-btn--block au-btn--green m-b-20" type="submit">register</button>
                        </form>
                        <div class="register-link">
                            <p>
                                ¿Ya tienes una cuenta
                                <a href="login.php">Login</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<?php
include_once ('views/footer.php');
?>
</body>
</html>
<?php
/**
 * Created by PhpStorm.
 * User: josuedjh
 * Date: 18-06-18
 * Time: 10:27 PM
 */
include_once('core/db.php');

class horarios
{
    private $pdo;
    public $id;
    public $entrada;
    public $salidad;
    public $detalle;


    public function __construct()
    {
        try{
            $this->pdo = Database::StartUp();
        }catch (Exception $e){
            die($e->getMessage());
        }
    }
    public function Listar()
    {
        try{
            $result = array();
            $stm = $this->pdo->prepare("SELECT * FROM horario");
            $stm->execute();
            return $stm->fetchAll(PDO::FETCH_OBJ);
        }catch (Exception $e){
            die($e->getMessage());
        }
    }

    public function Obtener($id){
        try {
            $stm = $this->pdo
                ->prepare("SELECT * FROM horario WHERE id = ?");
            $stm->execute(array($id));
            return $stm->fetch(PDO::FETCH_OBJ);
        }catch (Exception $e){
            die($e->getMessage());
        }
    }

    public function Eliminar($id)
    {
        try
        {
            $stm = $this->pdo
                ->prepare("DELETE FROM horario WHERE id = ?");

            $stm->execute(array($id));
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }
    public function Actualizar($data)
    {
        try {
            $sql = "UPDATE horario SET 
						entrada    = ?,
						salidad = ?,
						detalle  = ?,
				    	WHERE id	= ?";

            $this->pdo->prepare($sql)
                ->execute(
                    array(
                        $data->entrada,
                        $data->salidad,
                        $data->detalle,
                        $data->id
                    )
                );
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }
    public function Registrar(Horarios $data)
    {
        try
        {
            $sql = "INSERT INTO horario (entrada, salidad, detalle) 
		        VALUES (?, ?, ?)";

            $this->pdo->prepare($sql)
                ->execute(
                    array(
                        $data->entrada,
                        $data->salidad,
                        $data->detalle

                    )
                );
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }
}
<?php
include_once('core/db.php');

class eventos
{
    private $pdo;

    public $id;
    public $tipoevento;
    public $titulo;
    public $detalle;
    public $tipo;

    public function __CONSTRUCT()
    {
        try
        {
            $this->pdo = Database::StartUp();
        }
        catch(Exception $e)
        {
            die($e->getMessage());
        }
    }

    public function Listar()
    {
        try
        {
            $result = array();

            $stm = $this->pdo->prepare("SELECT * FROM eventos");
            $stm->execute();

            return $stm->fetchAll(PDO::FETCH_OBJ);
        }
        catch(Exception $e)
        {
            die($e->getMessage());
        }
    }

    public function Obtener($id)
    {
        try
        {
            $stm = $this->pdo
                ->prepare("SELECT * FROM eventos WHERE id = ?");
            $stm->execute(array($id));
            return $stm->fetch(PDO::FETCH_OBJ);
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }

    public function Eliminar($id)
    {
        try
        {
            $stm = $this->pdo
                ->prepare("DELETE FROM eventos WHERE id = ?");

            $stm->execute(array($id));
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }

    public function Actualizar($data)
    {
        try
        {
            $sql = "UPDATE eventos SET 
						tipoevento    = ?,
						titulo        = ?,
						detalle       = ?,
						tipo          = ?
				    	WHERE id	  = ?";

            $this->pdo->prepare($sql)
                ->execute(
                    array(
                        $data->tipoevento,
                        $data->titulo,
                        $data->detalle,
                        $data->tipo,
                        $data->id
                    )
                );
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }


    /**
     * @param Users $data
     */
    public function Registrar(Eventos $data)
    {
        try
        {
            $sql = "INSERT INTO eventos (tipoevento, titulo, detalle, tipo) 
		        VALUES (?, ?, ?, ?)";

            $this->pdo->prepare($sql)
                ->execute(
                    array(
                        $data->tipoevento,
                        $data->titulo,
                        $data->detalle,
                        $data->tipo

                    )
                );
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }

}
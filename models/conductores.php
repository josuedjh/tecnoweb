<?php
/**
 * Created by PhpStorm.
 * User: josuedjh
 * Date: 18-06-18
 * Time: 10:27 PM
 */
include_once('core/db.php');

class conductores
{
    private $pdo;
    public $id;
    public $nombre;
    public $apellido;
    public $celular;
    public $tipo;

    public function __construct()
    {
        try{
            $this->pdo = Database::StartUp();
        }catch (Exception $e){
            die($e->getMessage());
        }
    }
    public function Listar()
    {
        try{
            $result = array();
            $stm = $this->pdo->prepare("select * from conductores");
            $stm->execute();
            return $stm->fetchAll(PDO::FETCH_OBJ);
        }catch (Exception $e){
            die($e->getMessage());
        }
    }

    public function Obtener($id){
        try {
            $stm = $this->pdo
                ->prepare("SELECT * FROM conductores WHERE id = ?");
            $stm->execute(array($id));
            return $stm->fetch(PDO::FETCH_OBJ);
        }catch (Exception $e){
            die($e->getMessage());
        }
    }

    public function Eliminar($id)
    {
        try
        {
            $stm = $this->pdo
                ->prepare("DELETE FROM conductores WHERE id = ?");

            $stm->execute(array($id));
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }
    public function Actualizar($data)
    {
        try {
            $sql = "UPDATE conductores SET 
						nombre     = ?,
						apellido          = ?,
						celular           = ?,
						tipo           = ?
				    	WHERE id	    = ?";

            $this->pdo->prepare($sql)
                ->execute(
                    array(
                        $data->nombre,
                        $data->apellido,
                        $data->celular,
                        $data->tipo,
                        $data->id
                    )
                );
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }
    public function Registrar(Conductores $data)
    {
        try
        {
            $sql = "INSERT INTO conductores (nombre, apellido, celular, tipo) 
		        VALUES (?, ?, ?, ?)";

            $this->pdo->prepare($sql)
                ->execute(
                    array(
                        $data->nombre,
                        $data->apellido,
                        $data->celular,
                        $data->tipo,

                    )
                );
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: josuedjh
 * Date: 18-06-18
 * Time: 10:27 PM
 */
include_once('core/db.php');

class zonas
{
    private $pdo;
    public $id;
    public $nombre;
    public $capasidad;
    public $locacion;

    public function __construct()
    {
        try{
            $this->pdo = Database::StartUp();
        }catch (Exception $e){
            die($e->getMessage());
        }
    }
    public function Listar()
    {
        try{
            $result = array();
            $stm = $this->pdo->prepare("SELECT * FROM zonas");
            $stm->execute();
            return $stm->fetchAll(PDO::FETCH_OBJ);
        }catch (Exception $e){
            die($e->getMessage());
        }
    }

    public function Obtener($id){
        try {
            $stm = $this->pdo
                ->prepare("SELECT * FROM zonas WHERE id = ?");
            $stm->execute(array($id));
            return $stm->fetch(PDO::FETCH_OBJ);
        }catch (Exception $e){
            die($e->getMessage());
        }
    }

    public function Eliminar($id)
    {
        try
        {
            $stm = $this->pdo
                ->prepare("DELETE FROM zonas WHERE id = ?");

            $stm->execute(array($id));
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }
    public function Actualizar($data)
    {
        try {
            $sql = "UPDATE zonas SET 
						nombre    = ?,
						capasidad  = ?,
						locacion      = ?
				    	WHERE id	= ?";

            $this->pdo->prepare($sql)
                ->execute(
                    array(
                        $data->nombre,
                        $data->capasidad,
                        $data->locacion,
                        $data->id
                    )
                );
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }
    public function Registrar(Zonas $data)
    {
        try
        {
            $sql = "INSERT INTO zonas (nombre, capasidad, locacion) 
		        VALUES (?, ?, ?)";

            $this->pdo->prepare($sql)
                ->execute(
                    array(
                        $data->nombre,
                        $data->capasidad,
                        $data->locacion

                    )
                );
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }
}
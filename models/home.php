<?php
/**
 * Created by PhpStorm.
 * User: josuedjh
 * Date: 29-06-18
 * Time: 01:08 AM
 */
include_once('core/db.php');

class home
{
    private $pdo;
    public $id;

    public function __construct()
    {
        try{
            $this->pdo = Database::StartUp();
        }catch (Exception $e){
            die($e->getMessage());
        }
    }
    public function RepConductor()
    {
        try{
            $result = array();
            $stm = $this->pdo->prepare("select count(id) as id from conductores");
            $stm->execute();
            return $stm->fetchAll(PDO::FETCH_OBJ);
        }catch (Exception $e){
            die($e->getMessage());
        }
    }
    public function RepVehiculos()
    {
        try{
            $result = array();
            $stm = $this->pdo->prepare("select count(id) as id from vehiculos");
            $stm->execute();
            return $stm->fetchAll(PDO::FETCH_OBJ);
        }catch (Exception $e){
            die($e->getMessage());
        }
    }
    public function RepEntrada()
    {
        try{
            $result = array();
            $stm = $this->pdo->prepare("select count(id) as id from reguistro where estado='1';");
            $stm->execute();
            return $stm->fetchAll(PDO::FETCH_OBJ);
        }catch (Exception $e){
            die($e->getMessage());
        }
    }
    public function RepSalidad()
    {
        try{
            $result = array();
            $stm = $this->pdo->prepare("select count(id) as id from reguistro where estado='0';");
            $stm->execute();
            return $stm->fetchAll(PDO::FETCH_OBJ);
        }catch (Exception $e){
            die($e->getMessage());
        }
    }

}
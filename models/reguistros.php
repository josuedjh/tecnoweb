<?php
/**
 * Created by PhpStorm.
 * User: josuedjh
 * Date: 18-06-18
 * Time: 10:27 PM
 */
include_once('core/db.php');

class reguistros
{
    private $pdo;
    public $id;
    public $conductor;
    public $users;
    public $horario;
    public $estado;
    public $zona;

    public function __construct()
    {
        try{
            $this->pdo = Database::StartUp();
        }catch (Exception $e){
            die($e->getMessage());
        }
    }
    public function Listar()
    {
        try{
            $result = array();
            $stm = $this->pdo->prepare("SELECT * FROM reguistro");
            $stm->execute();
            return $stm->fetchAll(PDO::FETCH_OBJ);
        }catch (Exception $e){
            die($e->getMessage());
        }
    }

    public function Obtener($id){
        try {
            $stm = $this->pdo
                ->prepare("SELECT * FROM reguistro WHERE id = ?");
            $stm->execute(array($id));
            return $stm->fetch(PDO::FETCH_OBJ);
        }catch (Exception $e){
            die($e->getMessage());
        }
    }

    public function Eliminar($id)
    {
        try
        {
            $stm = $this->pdo
                ->prepare("DELETE FROM reguistro WHERE id = ?");

            $stm->execute(array($id));
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }
    public function Actualizar($data)
    {
        try {
            $sql = "UPDATE reguistro SET 
						conductor    = ?,
						users  = ?,
						horario      = ?,
						estado  = ?,
						zona = ?
				    	WHERE id	= ?";

            $this->pdo->prepare($sql)
                ->execute(
                    array(
                        $data->conductor,
                        $data->users,
                        $data->horario,
                        $data->estado,
                        $data->zona,
                        $data->id
                    )
                );
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }
    public function Registrar(Reguistros $data)
    {
        try
        {
            $sql = "INSERT INTO reguistro (conductor, users, horario, estado, zona) 
		        VALUES (?, ?, ?, ?, ?)";

            $this->pdo->prepare($sql)
                ->execute(
                    array(
                        $data->conductor,
                        $data->users,
                        $data->horario,
                        $data->estado,
                        $data->zona

                    )
                );
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }
}
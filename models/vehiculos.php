<?php
/**
 * Created by PhpStorm.
 * User: josuedjh
 * Date: 18-06-18
 * Time: 10:27 PM
 */
include_once('core/db.php');

class vehiculos
{
    private $pdo;
    public $id;
    public $idconductor;
    public $modelo;
    public $placa;
    public $color;

    public function __construct()
    {
        try{
            $this->pdo = Database::StartUp();
        }catch (Exception $e){
            die($e->getMessage());
        }
    }
    public function Listar()
    {
        try{
            $result = array();
            $stm = $this->pdo->prepare("select conductores.nombre as idconductor, vehiculos.modelo as modelo, vehiculos.placa as placa, vehiculos.color as color from vehiculos, conductores
where vehiculos.idconductor = conductores.id");
            $stm->execute();
            return $stm->fetchAll(PDO::FETCH_OBJ);
        }catch (Exception $e){
            die($e->getMessage());
        }
    }

    public function Obtener($id){
        try {
            $stm = $this->pdo
                ->prepare("SELECT * FROM vehiculos WHERE id = ?");
            $stm->execute(array($id));
            return $stm->fetch(PDO::FETCH_OBJ);
        }catch (Exception $e){
            die($e->getMessage());
        }
    }

    public function Eliminar($id)
    {
        try
        {
            $stm = $this->pdo
                ->prepare("DELETE FROM vehiculos WHERE id = ?");

            $stm->execute(array($id));
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }
    public function Actualizar($data)
    {
        try {
            $sql = "UPDATE vehiculos SET 
						idconductor     = ?,
						modelo          = ?,
						placa           = ?,
						color           = ?
				    	WHERE id	    = ?";

            $this->pdo->prepare($sql)
                ->execute(
                    array(
                        $data->idconductor,
                        $data->modelo,
                        $data->placa,
                        $data->color,
                        $data->id
                    )
                );
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }
    public function Registrar(Vehiculos $data)
    {
        try
        {
            $sql = "INSERT INTO vehiculos (idconductor, modelo, placa, color) 
		        VALUES (?, ?, ?, ?)";

            $this->pdo->prepare($sql)
                ->execute(
                    array(
                        $data->idconductor,
                        $data->modelo,
                        $data->placa,
                        $data->color,

                    )
                );
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }
}
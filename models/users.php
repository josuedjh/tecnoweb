<?php
include_once('core/db.php');

class Users
{
    private $pdo;

    public $id;
    public $nombre;
    public $apellido;
    public $tipo;

    public function __CONSTRUCT()
    {
        try
        {
            $this->pdo = Database::StartUp();
        }
        catch(Exception $e)
        {
            die($e->getMessage());
        }
    }

    public function Listar()
    {
        try
        {
            $result = array();

            $stm = $this->pdo->prepare("SELECT * FROM users");
            $stm->execute();

            return $stm->fetchAll(PDO::FETCH_OBJ);
        }
        catch(Exception $e)
        {
            die($e->getMessage());
        }
    }

    public function Obtener($id)
    {
        try
        {
            $stm = $this->pdo
                ->prepare("SELECT * FROM users WHERE id = ?");
            $stm->execute(array($id));
            return $stm->fetch(PDO::FETCH_OBJ);
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }

    public function Eliminar($id)
    {
        try
        {
            $stm = $this->pdo
                ->prepare("DELETE FROM users WHERE id = ?");

            $stm->execute(array($id));
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }

    public function Actualizar($data)
    {
        try
        {
            $sql = "UPDATE users SET 
						email    = ?,
						nombre  = ?,
						apellido      = ?,
						thema      = ?,
						tipo      = ?
				    	WHERE id	= ?";

            $this->pdo->prepare($sql)
                ->execute(
                    array(
                        $data->email,
                        $data->nombre,
                        $data->apellido,
                        $data->thema,
                        $data->tipo,
                        $data->id
                    )
                );
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }


    /**
     * @param Users $data
     */
    public function Registrar(Users $data)
    {
        try
        {
            $sql = "INSERT INTO Users (nombre, apellido, tipo) 
		        VALUES (?, ?)";

            $this->pdo->prepare($sql)
                ->execute(
                    array(
                        $data->tipo,
                        $data->nombre,
                        $data->apellido

                    )
                );
        } catch (Exception $e)
        {
            die($e->getMessage());
        }
    }

}
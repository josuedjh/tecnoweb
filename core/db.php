<?php
class Database{
    public static function StartUp()
    {
        $host = "127.0.0.1";
        $port = "5433";
        $dbname = "myapp";
        $dbuser = "userapp";
        $dbpass = "123456";

        $pdo = new PDO("pgsql:dbname=$dbname;host=$host;port=$port", $dbuser, $dbpass);

        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $pdo;
    }
}
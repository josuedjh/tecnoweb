<?php
error_reporting(0);
?>
<div class="row">
    <div class="col-md-12">
        <!-- USER DATA-->
        <div class="user-data m-b-30">
            <h3 class="title-3 m-b-30">
                <i class="zmdi zmdi-account-calendar"></i>Hola </h3>
            <div class="filters m-b-45">
            </div>
            <div class="table-responsive table-data">
                <table class="table table-data2">
                    <thead>
                    <tr>
                        <th>conductor</th>
                        <th>users</th>
                        <th>horario</th>
                        <th>estado</th>
                        <th>zona</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach($this->model->Listar() as $r): ?>
                        <tr class="tr-shadow">
                            <td>
                                <spam><?php echo $r->conductor; ?></spam>
                            </td>
                            <td>
                                <span><?php echo $r->users; ?></span>
                            </td>
                            <td>
                                <spam><?php echo $r->horario; ?></spam>
                            </td>
                            <td>
                                <spam><?php echo $r->estado; ?></spam>
                            </td>
                            <td>
                                <spam><?php echo $r->zona; ?></spam>
                            </td>
                            <td>
                                <div class="table-data-feature">
                                    <button class="item" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit">
                                        <a href="?c=reguistros&a=Crud&id=<?php echo $r->id; ?>"><i class="zmdi zmdi-edit"></i></a>
                                    </button>
                                    <button class="item" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete">
                                        <a href="?c=reguistros&a=Eliminar&id=<?php echo $r->id; ?>"><i class="zmdi zmdi-delete"></i></a>
                                    </button>
                                </div>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
            <div class="card">
                <div class="card-body card-block">
                    <div class="card">
                        <div class="card-header">Actulizar y Reguistrar</div>
                        <div class="card-body">
                            <div class="card-title">
                                <h3 class="text-center title-2">Zonas de estacionamiento</h3>
                            </div>
                            <hr>
                            <form action="?c=reguistros&a=Guardar" method="post" enctype="multipart/form-data" novalidate="novalidate">
                                <div class="form-group invisible">
                                    <label for="cc-payment" class="control-label mb-1">id</label>
                                    <input name="id" type="number" class="form-control" value="<?php echo $reguistros->id; ?>">
                                </div>
                                <div class="form-group">
                                    <label for="cc-payment" class="control-label mb-1">Conductor</label>
                                    <select type="text" name="conductor" value="<?php echo $reguistros->conductor; ?>" class="form-control">
                                        <?php foreach($this->modelsconductores->Listar() as $h): ?>
                                            <option value="<?php echo $h->id; ?>"><?php echo $h->nombre; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="cc-exp" class="control-label mb-1">User</label>
                                            <input name="users" type="number" class="form-control cc-exp" value="<?php echo $reguistros->users; ?>">
                                            <span class="help-block"></span>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <label for="x_card_code" class="control-label mb-1">Horario</label>
                                        <select type="text" name="horario" value="<?php echo $reguistros->horario; ?>" class="form-control">
                                            <?php foreach($this->modelshorarios->Listar() as $h): ?>
                                                <option value="<?php echo $h->id; ?>"><?php echo $h->entrada; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <!--<div class="form-group">
                                            <label for="cc-exp" class="control-label mb-1">Estado</label>
                                            <input name="estado" type="number" class="form-control cc-exp" value="<?php /*echo $reguistros->estado; */?>">
                                            <span class="help-block"></span>
                                        </div>-->
                                        <div class="row form-group">
                                            <div class="col col-md-6">
                                                <label class=" form-control-label">su estado actual es: <?php /*echo $reguistros->estado; */?></label>
                                            </div>
                                            <div class="col col-md-6">
                                                <div class="form-check">
                                                    <div class="radio">
                                                        <label for="radio1" class="form-check-label ">
                                                            <input type="radio" id="radio1" name="estado" value="1" class="form-check-input">Entrando
                                                        </label>
                                                    </div>
                                                    <div class="radio">
                                                        <label for="radio2" class="form-check-label ">
                                                            <input type="radio" id="radio2" name="estado" value="0" class="form-check-input">Saliendo
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <label for="x_card_code" class="control-label mb-1">Zona</label>
                                        <div class="input-group">
                                            <select type="text" name="zona" value="<?php echo $reguistros->zona; ?>" class="form-control">
                                                <?php foreach($this->modelszonas->Listar() as $h): ?>
                                                    <option value="<?php echo $h->id; ?>"><?php echo $h->nombre; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <hr>
                                    <button type="submit" class="btn btn-lg btn-info btn-block">
                                        <i class="fa fa-lock fa-lg"></i>&nbsp;
                                        <span id="payment-button-amount">Guardar</span>
                                        <span id="payment-button-sending" style="display:none;">Sending…</span>
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="user-data__footer">
                <button class="au-btn au-btn-load">load more</button>
            </div>
        </div>
        <!-- END USER DATA-->
    </div>
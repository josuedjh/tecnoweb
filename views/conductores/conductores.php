<?php
error_reporting(0);
?>
<div class="row">
    <div class="col-md-12">
        <!-- USER DATA-->
        <div class="user-data m-b-30">
            <h3 class="title-3 m-b-30">
                <i class="zmdi zmdi-account-calendar"></i>Hola </h3>
            <div class="filters m-b-45">
            </div>
            <div class="table-responsive table-data">
                <table class="table table-data2">
                    <thead>
                    <tr>
                        <th>nombre del conductor</th>
                        <th>apellido</th>
                        <th>Celular</th>
                        <th>tipo</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach($this->model->Listar() as $r): ?>
                        <tr class="tr-shadow">
                            <td><?php echo $r->nombre; ?></td>
                            <td>
                                <span class="block-email"><?php echo $r->apellido; ?></span>
                            </td>
                            <td class="desc"><?php echo $r->celular; ?></td>
                            <td>
                                <spam class="block-email"><?php echo $r->tipo; ?></spam>
                            </td>
                            <td>
                                <div class="table-data-feature">
                                    <button class="item" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit">
                                        <a href="?c=conductores&a=Crud&id=<?php echo $r->id; ?>"><i class="zmdi zmdi-edit"></i></a>
                                    </button>
                                    <button class="item" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete">
                                        <a href="?c=conductores&a=Eliminar&id=<?php echo $r->id; ?>"><i class="zmdi zmdi-delete"></i></a>
                                    </button>
                                </div>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
            <div class="card">
                <div class="card-body card-block">
                    <div class="card">
                        <div class="card-header">Actulizar y Reguistrar</div>
                        <div class="card-body">
                            <div class="card-title">
                                <h3 class="text-center title-2">Zonas de estacionamiento</h3>
                            </div>
                            <hr>
                            <form action="?c=conductores&a=Guardar" method="post" enctype="multipart/form-data" novalidate="novalidate">
                                <div class="form-group invisible">
                                    <label for="cc-payment" class="control-label mb-1">id</label>
                                    <input name="id" type="number" class="form-control" value="<?php echo $conductores->id; ?>">
                                </div>
                                <div class="form-group">
                                    <label for="cc-payment" class="control-label mb-1">Nombre del conductor</label>
                                    <input name="nombre" type="text" class="form-control" aria-required="true" value="<?php echo $conductores->nombre; ?>">
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="cc-exp" class="control-label mb-1">Apellido</label>
                                            <input name="apellido" type="text" class="form-control cc-exp" value="<?php echo $conductores->apellido; ?>">
                                            <span class="help-block"></span>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <label for="x_card_code" class="control-label mb-1">Celular</label>
                                        <div class="input-group">
                                            <input name="celular" type="text" class="form-control cc-cvc" value="<?php echo $conductores->celular; ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="cc-payment" class="control-label mb-1">Tipo</label>
                                    <input name="tipo" type="text" class="form-control" aria-required="true" value="<?php echo $conductores->tipo; ?>">
                                </div>
                                <div>
                                    <button type="submit" class="btn btn-lg btn-info btn-block">
                                        <i class="fa fa-lock fa-lg"></i>&nbsp;
                                        <span id="payment-button-amount">Guardar</span>
                                        <span id="payment-button-sending" style="display:none;">Sending…</span>
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="user-data__footer">
                <button class="au-btn au-btn-load">load more</button>
            </div>
        </div>
        <!-- END USER DATA-->
    </div>




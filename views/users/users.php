<?php
error_reporting(0);
?>
<div class="row">
    <div class="col-md-12">
        <!-- USER DATA-->
        <div class="user-data m-b-30">
            <h3 class="title-3 m-b-30">
                <i class="zmdi zmdi-account-calendar"></i>Hola </h3>
            <div class="filters m-b-45">
            </div>
            <div class="table-responsive table-data">
                <table class="table" style="width: 100%">
                    <thead>
                    <tr>
                        <td>Nombre</td>
                        <td>Roles</td>
                        <td>accion</td>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach($this->model->Listar() as $r): ?>
                    <tr>
                        <td>
                            <div class="table-data__info">
                                <h6><?php echo $r->nombre; ?> <?php echo $r->apellido; ?></h6>
                                <span>
                                    <a href="#"><?php echo $r->email; ?></a>
                                 </span>
                            </div>
                        </td>
                        <td>
                            <span class="role <?php echo $r->tipo; ?>"><?php echo $r->tipo; ?></span>
                        </td>
                        <td>
                            <a class="btn btn-outline-primary" href="?c=users&a=Crud&id=<?php echo $r->id; ?>">Editar</a>
                            <a class="btn btn-outline-secondary" onclick="javascript:return confirm('¿Seguro de eliminar este registro?');" href="?c=users&a=Eliminar&id=<?php echo $r->id; ?>">Eliminar</a>
                        </td>
                    </tr>

                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
            <div class="card">
                <div class="card-header">
                    <strong>User</strong> Form
                </div>
                <div class="card-body card-block">
                    <form action="?c=users&a=Guardar" method="post" enctype="multipart/form-data">
                        <div class="form-group invisible">
                            <label for="exampleInputName2" class="px-1 form-control-label">id del usuario</label>
                            <input type="text" id="exampleInputName2" name="id" value="<?php echo $users->id; ?>" required="" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail2" class="px-1 form-control-label">Email</label>
                            <input type="email" id="exampleInputEmail2" name="email" value="<?php echo $users->email; ?>" required="" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail2" class="px-1 form-control-label">Nombre</label>
                            <input type="name" id="exampleInputEmail2" name="nombre" value="<?php echo $users->nombre; ?>" required="" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail2" class="px-1 form-control-label">Apellido</label>
                            <input type="name" id="exampleInputEmail2" name="apellido" value="<?php echo $users->apellido; ?>" class="form-control">
                        </div>
                        <div class="form-group">
                            <div class="row form-group">
                                <div class="col col-md-4">
                                    <label class=" form-control-label">Selecione el thema</label>
                                </div>
                                <div class="col col-md-8">
                                    <div class="form-check-inline form-check">
                                        <label for="inline-radio1" class="form-check-label ">
                                            <input type="radio" id="inline-radio1" name="thema" value="theme" class="form-check-input">Blanco
                                        </label>
                                        <label for="inline-radio2" class="form-check-label ">
                                            <input type="radio" id="inline-radio2" name="thema" value="azul" class="form-check-input">Azul
                                        </label>
                                        <label for="inline-radio3" class="form-check-label ">
                                            <input type="radio" id="inline-radio3" name="thema" value="dark" class="form-check-input">Dark
                                        </label>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3">
                                <label class=" form-control-label">Tipo de Usuario</label>
                            </div>
                            <div class="col col-md-9">
                                <div class="form-check">
                                    <div class="radio">
                                        <label for="radio1" class="form-check-label ">
                                            <input type="radio" id="radio1" name="tipo" value="admin" class="form-check-input">Administrador
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label for="radio2" class="form-check-label ">
                                            <input type="radio" id="radio2" name="tipo" value="user" class="form-check-input">Users
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary btn-sm">
                                <i class="fa fa-dot-circle-o"></i> Guardar
                            </button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="user-data__footer">
                <button class="au-btn au-btn-load">load more</button>
            </div>
        </div>
        <!-- END USER DATA-->
    </div>




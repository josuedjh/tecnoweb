        <!-- MENU SIDEBAR-->
        <aside class="menu-sidebar d-none d-lg-block">
            <div class="logo">
                <a href="#">
                    <img src="static/images/icon/logo.png" alt="Cool Admin" />
                </a>
            </div>
            <div class="menu-sidebar__content js-scrollbar1">
                <nav class="navbar-sidebar">
                    <ul class="list-unstyled navbar__list">
                        <li class="active has-sub">
                            <a href="#">
                                <i class="fas fa-tachometer-alt"></i>Dashboard</a>
                        </li>
                        <?php
                        $admin1 = "admin";
                        $user1 = $user['tipo'];
                        if ($admin1 == $user1){
                            echo "
                        <li>
                            <a href=\"?c=users\">
                                <i class=\"fas fa-chart-bar\"></i>User</a>
                        </li>
                        <li>
                            <a href=\"?c=reguistros\">
                                <i class=\"fas fa-chart-bar\"></i>Reguistro</a>
                        </li>
                        <li>
                            <a href=\"?c=conductores\">
                                <i class=\"far fa-check-square\"></i>Conductores</a>
                        </li>
                        <li>
                            <a href=\"?c=vehiculos\">
                                <i class=\"fas fa-calendar-alt\"></i>Vehiculos</a>
                        </li>
                        <li>
                            <a href=\"?c=zonas\">
                                <i class=\"fas fa-map-marker-alt\"></i>Zonas</a>
                        </li>
                        <li>
                            <a href=\"?c=horarios\">
                                <i class=\"fas fa-map-marker-alt\"></i>Horarios</a>
                        </li>
                            ";
                        }elseif ($admin1 != $user1){
                            echo "
                            <li>
                            <a href=\"chart.html\">
                                <i class=\"fas fa-chart-bar\"></i>Reguistro</a>
                        </li>
                        <li>
                            <a href=\"form.html\">
                                <i class=\"far fa-check-square\"></i>Conductores</a>
                        </li>
                        <li>
                            <a href=\"?c=vehiculos\">
                                <i class=\"fas fa-calendar-alt\"></i>Vehiculos</a>
                        </li>
                            ";
                        }else{
                            echo "no permitido";
                        }
                        ?>
                    </ul>
                </nav>
            </div>
        </aside>
        <!-- END MENU SIDEBAR-->
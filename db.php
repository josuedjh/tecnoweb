<?php
$host = "127.0.0.1";
$port = "5433";
$dbname = "myapp";
$dbuser = "userapp";
$dbpass = "123456";

try{
    $conn = new PDO("pgsql:dbname=$dbname;host=$host;port=$port", $dbuser, $dbpass);
} catch(PDOException $e){
    die( "Connection failed: " . $e->getMessage());
}
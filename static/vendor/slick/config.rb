css_dir = "."
sass_dir = "."
static/images_dir = "."
fonts_dir = "fonts"
relative_assets = true

output_style = :compact
line_comments = false

preferred_syntax = :scss
<!DOCTYPE html>
<html lang="en">
<head>
    <?php
   ob_start();
    ?>
<?php

session_start();
require 'db.php';


if( isset($_SESSION['user_id']) ){

    $records = $conn->prepare('SELECT id,email,password,nombre,apellido,thema,tipo FROM users WHERE id = :id');
    $records->bindParam(':id', $_SESSION['user_id']);
    $records->execute();
    $results = $records->fetch(PDO::FETCH_ASSOC);

    $user = NULL;

    if( count($results) > 0){
        $user = $results;
    }

}

include_once ('views/header.php');

?>
</head>
<body class="animsition">
<?php
header("Content-Type: text/html; charset=UTF-8");
?>
    <div class="page-wrapper">
<?php
    include_once ('views/mobil.php');
?>
<?php
    include_once ('views/sidebar.php');
?>
        <div class="page-container">
            <header class="header-desktop">
                <?php
                    include_once ('views/desktop.php');
                ?>
            </header>
            <div class="main-content">
                <div class="section__content section__content--p30">
                <div class="col-md-12">
                    <div>
                        <?php
                        $controller = 'home';
                        require_once 'core/shared.php';
                        ?>
                        <button class="au-btn au-btn-icon au-btn--blue">
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
<div id="sfcxujsecxrej6cqcxxl49l9wnsrh8p1lun"></div>
<script type="text/javascript" src="https://counter11.whocame.ovh/private/counter.js?c=xujsecxrej6cqcxxl49l9wnsrh8p1lun&down=async" async></script>
<noscript><a href="https://www.contadorvisitasgratis.com" title="contador de visitas"><img src="https://counter11.whocame.ovh/private/contadorvisitasgratis.php?c=xujsecxrej6cqcxxl49l9wnsrh8p1lun" border="0" title="contador de visitas" alt="contador de visitas"></a></noscript>
<?php 
  include_once ('views/footer.php');
?>
<?php
ob_end_flush();
?>
</body>
</html>